<?php

namespace Uagc;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

/**
 * GrouperApiClient
 *
 * A client for the Stache API
 *
 * @method object get(string $item, string $key)
 *
 */
class GrouperApiClient
{
    /**
     * @var $baseUrl string The host URL of the Grouper API.
     */
    private $baseUrl;

    /**
     * @var $membersTpt string The URL path template of the `members` API call.
     */
    private $membersTpt;

    /**
     * @var $membershipsTpt string The URL path template of the `memberships` API call.
     */
    private $membershipsTpt;

    /**
     * @var $modifyTpt string The URL path template for adding & removing group memberships.
     */
    private $modifyTpt;

    /**
     * @var $client object A Guzzle client
     */
    private $client;

    /**
     * @var $requestOptions array Auth & header options for the Guzzle Client
     */
    private $requestOptions;

    /**
     * The constructor takes a parameter array of at least the following:
     * - user: (string) The username of the Grouper API client credentials. Required
     * - password: (string) The password of the Grouper API client credentials. Required
     * - baseUrl: (string) The host URL of the Grouper API. Required
     *
     * Options parameters include:
     * - timeout: (int) Seconds to wait before the request times out. Defaults to 30
     * - ssl_key: (string) Filesystem path to a PEM certificate for the Grouper API host. Defaults to null
     * - user_agent: (string) User-Agent header string. Defaults to "UA Graduate College Grouper Client for PHP"
     *
     * @param array $params Client configuration settings
     */
    public function __construct($params = array())
    {
        $this->baseUrl = !empty($params['baseUrl']) ?
            $params['baseUrl'] : null;

        $this->membersTpt = !empty($params['membersTpt']) ?
            $params['membersTpt'] : '/groups/%1$s/members';

        $this->membershipsTpt = !empty($params['membershipsTpt']) ?
            $params['membershipsTpt'] : '/subjects/%1$s/memberships';

        $this->modifyTpt = !empty($params['modifyTpt']) ?
            $params['modifyTpt'] : '/groups/%1$s/members/%2$s';

        if (empty($this->baseUrl) || !is_string($this->baseUrl)) {
            throw new \InvalidArgumentException('baseUrl is invalid');
        }
        if (empty($this->membersTpt) || !is_string($this->membersTpt)) {
            throw new \InvalidArgumentException('membersTpt is invalid');
        }
        if (empty($this->membershipsTpt) || !is_string($this->membershipsTpt)) {
            throw new \InvalidArgumentException('membershipsTpt is invalid');
        }
        if (empty($this->modifyTpt) || !is_string($this->modifyTpt)) {
            throw new \InvalidArgumentException('modifyTpt is invalid');
        }

        $timeout = !empty($params['timeout']) ? $params['timeout'] : 60;
        $authUser = !empty($params['auth_user']) ? $params['auth_user'] : null;
        $authPw = !empty($params['auth_pw']) ? $params['auth_pw'] : null;
        $caCert = !empty($params['ssl_key']) ? $params['ssl_key'] : null;
        $userAgent = !empty($params['user_agent']) ?
            $params['user_agent'] : 'UA Graduate College Grouper Client for PHP';

        if (empty($timeout) || !is_numeric($timeout)) {
            throw new \InvalidArgumentException('timeout is invalid');
        }
        if (empty($authUser) || !is_string($authUser)) {
            throw new \InvalidArgumentException('authUser is invalid');
        }
        if (empty($authPw) || !is_string($authPw)) {
            throw new \InvalidArgumentException('authPw is invalid');
        }
        if (empty($userAgent) || !is_string($userAgent)) {
            throw new \InvalidArgumentException('userAgent is invalid');
        }

        $this->requestOptions = [
            'auth' => [
                $authUser,
                $authPw
            ],
            'connect_timeout' => $timeout,
            'headers' => [
                'User-Agent' => $userAgent
            ],
            'ssl_key' => $caCert,
            'timeout' => $timeout,
        ];

        $this->client = new \GuzzleHttp\Client();
    }

    /**
     * Query members of a group
     *
     * @param string $groupId - The full path ID of a Grouper group
     *
     * @return array Assosiative array of memberships by 'people' and 'groups'
     */
    public function getMembers($groupId)
    {
        $requestUrl = sprintf(
            $this->baseUrl . $this->membersTpt,
            $groupId
        );
        $response = $this->client->request(
            'GET',
            $requestUrl,
            $this->requestOptions
        );

        $responseStatus = $response->getStatusCode();
        $responseBody = $response->getBody();

        if ($responseStatus !== 200) {
            throw new \Exception(
                'Error ' . $responseStatus . ': ' . $responseBody
            );
        }

        $members = [
            'people' => [],
            'groups' => []
        ];
        $responseObj = json_decode((string) $responseBody);
        foreach ($responseObj->WsGetMembersLiteResult->wsSubjects as $nextMember) {
            if ($nextMember->sourceId === 'ldap') {
                $members['people'][] = $nextMember->id;
            }
            if ($nextMember->sourceId === 'g:gsa') {
                $members['groups'][] = $nextMember->id;
            }
        }

        return $members;
    }

    /**
     * Query whether group has a member
     *
     * @param string $groupId - The full path ID of a Grouper group
     * @param string $subjectId - The subject ID, either a person's 'uaid' or a group's 'uuid'
     *
     * @return boolean True if the subject is a member of the group. Otherwise false.
     */
    public function hasMember($groupId, $subjectId)
    {
        $requestUrl = sprintf(
            $this->baseUrl . $this->modifyTpt,
            $groupId,
            $subjectId
        );
        $response = $this->client->request(
            'GET',
            $requestUrl,
            $this->requestOptions
        );

        $responseStatus = $response->getStatusCode();
        $responseBody = $response->getBody();

        if ($responseStatus !== 200) {
            throw new \Exception(
                'Error ' . $responseStatus . ': ' . $responseBody
            );
        }

        $responseObj = json_decode((string) $responseBody);
        $resultCode = $responseObj->WsHasMemberLiteResult->resultMetadata->resultCode;
        if ($resultCode === 'IS_MEMBER') {
            return true;
        }

        return false;
    }

    /**
     * Query a user's memberships
     *
     * @param string $subjectId A person's 'uaid' or a group's 'uuid'
     *
     * @return array an Associative array of the subject's memberships
     */
    public function getMemberships($subjectId)
    {
        $requestUrl = sprintf(
            $this->baseUrl . $this->membershipsTpt,
            $subjectId
        );
        $response = $this->client->request(
            'GET',
            $requestUrl,
            $this->requestOptions
        );

        $responseStatus = $response->getStatusCode();
        $responseBody = $response->getBody();

        if ($responseStatus !== 200) {
            throw new \Exception(
                'Error ' . $responseStatus . ': ' . $responseBody
            );
        }

        $memberships = [
            'subject' => $subjectId,
            'groups' => []
        ];
        $responseObj = json_decode((string) $responseBody);

        if (isset($responseObj->WsGetMembershipsResults->wsGroups)) {
            foreach ($responseObj->WsGetMembershipsResults->wsGroups as $nextGroup) {
                $memberships['groups'][] = [
                    'name' => $nextGroup->name,
                    'uuid' => $nextGroup->uuid
                ];
            }
        }

        return $memberships;
    }

    /**
     * Add a user to a group
     *
     * @param string $groupId - The full path ID of the Grouper group
     * @param string $subjectId - A person's 'uaid' or a group's 'uuid'
     *
     * @return array Associative array indicating 'success', 'message', 'group', and 'subject'
     */
    public function addToGroup($groupId, $subjectId)
    {
        $requestUrl = sprintf(
            $this->baseUrl . $this->modifyTpt,
            $groupId,
            $subjectId
        );
        $response = $this->client->request(
            'PUT',
            $requestUrl,
            $this->requestOptions
        );

        $responseStatus = $response->getStatusCode();
        $responseBody = $response->getBody();

        if ($responseStatus === 200) {
            return [
                'success' => true,
                'message' => 'SUCCESS: Member already existed',
                'subject' => $subjectId,
                'group' => $groupId
            ];
        } else if ($responseStatus === 201) {
            return [
                'success' => true,
                'message' => 'SUCCESS: Member added',
                'subject' => $subjectId,
                'group' => $groupId
            ];
        } else {
            throw new \Exception(
                'Error ' . $responseStatus . ': ' . $responseBody
            );
        }
    }

    /**
     * Remove a user from a group
     *
     * @param string $groupId The full path ID of the Grouper group
     * @param string $subjectId A person's 'uaid' or a group's 'uuid'
     *
     * @return array Associative array indicating 'success', 'message', 'group', and 'subject'
     */
    public function removeFromGroup($groupId, $subjectId)
    {
        $requestUrl = sprintf(
            $this->baseUrl . $this->modifyTpt,
            $groupId,
            $subjectId
        );
        $response = $this->client->request(
            'DELETE',
            $requestUrl,
            $this->requestOptions
        );

        $responseStatus = $response->getStatusCode();
        $responseBody = $response->getBody();
        $responseObj = json_decode((string) $responseBody);

        if ($responseStatus !== 200) {
            throw new \Exception(
                'Error ' . $responseStatus . ': ' . $responseBody
            );
        }

        return [
            'success' => true,
            'message' => 'SUCCESS: Member was removed or was not a member',
            'subject' => $subjectId,
            'group' => $groupId
        ];
    }
}
