<?php

use PHPUnit\Framework\TestCase;

class GrouperApiClientTest extends TestCase
{

    public function testCanBeInstantiatedWithValidParams()
    {
        $this->assertInstanceOf(
            Uagc\GrouperApiClient::class,
            new Uagc\GrouperApiClient([
                'auth_user' => 'user_string',
                'auth_pw' => 'a-V3ry_veRY:Str0Ng#p4w0rD!',
                'baseUrl' => 'https://grouper.domain.edu/grouper-ws/servicesRest/json/v2_1_005'
            ])
        );
    }

    public function testCannotBeInstantiatedWithNoParams()
    {
        // at miminum, 'domain' must be passed
        $this->setExpectedException(InvalidArgumentException::class);
        new Uagc\GrouperApiClient();
    }

    public function testCannotBeInstantiatedWithInvalidParams()
    {
        // baseUrl must be a non-empty string
        $this->setExpectedException(InvalidArgumentException::class);
        new Uagc\GrouperApiClient(['domain' => null]);

        // authUser must be a non-empty string
        $this->setExpectedException(InvalidArgumentException::class);
        new Uagc\GrouperApiClient(['authUser' => null]);

        // authPw must be a non-empty string
        $this->setExpectedException(InvalidArgumentException::class);
        new Uagc\GrouperApiClient(['authPw' => null]);

        // timeout must be numeric
        $this->setExpectedException(InvalidArgumentException::class);
        new Uagc\GrouperApiClient(['timeout' => 'invalid']);

        // user_agent must be a string (or empty value)
        $this->setExpectedException(InvalidArgumentException::class);
        new Uagc\GrouperApiClient(['user_agent' => ['invalid' => 'invalid']]);
    }

    /*
     * The remaining tests are only accurate if these environment vars are set
     * - GROUPER_TEST_BASEURL
     * - GROUPER_TEST_AUTHUSER
     * - GROUPER_TEST_AUTHPW
     * - GROUPER_TEST_GROUP
     * - GROUPER_TEST_SUBJECT
     */

    // It helps to start by populating the test group with the test subject
    public function testAddToGroupWorksWithValidArgs()
    {
        if ($this->canTestLive()) {
            $gac = $this->getClient();
            $response = $gac->addToGroup(
                getenv('GROUPER_TEST_GROUP'),
                getenv('GROUPER_TEST_SUBJECT')
            );
            $this->assertTrue(
                is_array($response)
            );
        }
    }

    public function testAddToGroupFailsWithoutValidArgs()
    {
        if ($this->canTestLive()) {
            $gac = $this->getClient();
            $this->setExpectedException(Exception::class);
            $gac->addToGroup('invalid', 'invalid');
        }
    }

    public function testGetMembersWorksWithValidArgs()
    {
        if ($this->canTestLive()) {
            $gac = $this->getClient();
            $response = $gac->getMembers(
                getenv('GROUPER_TEST_GROUP')
            );
            $this->assertTrue(
                is_array($response)
            );
        }
    }

    public function testGetMembersFailsWithoutValidArgs()
    {
        if ($this->canTestLive()) {
            $gac = $this->getClient();
            $this->setExpectedException(Exception::class);
            $gac->getMembers('invalid');
        }
    }

    public function testHasMemberWorksWithValidArgs()
    {
        if ($this->canTestLive()) {
            $gac = $this->getClient();

            // get
            $response = $gac->hasMember(
                getenv('GROUPER_TEST_GROUP'),
                getenv('GROUPER_TEST_SUBJECT')
            );
            $this->assertTrue(
                $response
            );
        }
    }

    public function testHasMemberFailsWithoutValidArgs()
    {
        if ($this->canTestLive()) {
            $gac = $this->getClient();
            $this->setExpectedException(Exception::class);
            $gac->hasMember('invalid', 'invalid');
        }
    }

    public function testGetMembershipsWorksWithValidArgs()
    {
        if ($this->canTestLive()) {
            $gac = $this->getClient();
            $response = $gac->getMemberships(
                getenv('GROUPER_TEST_SUBJECT')
            );
            $this->assertTrue(
                is_array($response)
            );
        }
    }

    public function testRemoveFromGroupWorksWithValidArgs()
    {
        if ($this->canTestLive()) {
            $gac = $this->getClient();
            $response = $gac->removeFromGroup(
                getenv('GROUPER_TEST_GROUP'),
                getenv('GROUPER_TEST_SUBJECT')
            );
            $this->assertTrue(is_array($response));
            $this->assertTrue(
                isset($response['success']) && isset($response['message'])
            );
            $this->assertTrue($response['success']);
        }
    }

    public function testRemoveFromGroupFailsWithoutValidArgs()
    {
        if ($this->canTestLive()) {
            $gac = $this->getClient();
            $this->setExpectedException(Exception::class);
            $gac->removeFromGroup('invalid', 'invalid');
        }
    }

    // Some tests should only run if these env vars are set
    private function canTestLive()
    {
        return getenv('GROUPER_TEST_BASEURL') &&
            getenv('GROUPER_TEST_AUTHUSER') &&
            getenv('GROUPER_TEST_AUTHPW') &&
            getenv('GROUPER_TEST_GROUP') &&
            getenv('GROUPER_TEST_SUBJECT');
    }

    private function getClient()
    {
        return new Uagc\GrouperApiClient([
            'baseUrl' => getenv('GROUPER_TEST_BASEURL'),
            'auth_user' => getenv('GROUPER_TEST_AUTHUSER'),
            'auth_pw' => getenv('GROUPER_TEST_AUTHPW')
        ]);
    }
}
