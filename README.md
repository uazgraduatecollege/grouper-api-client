# Grouper API Client for PHP

A simple PHP package for interacting with the Grouper API

## Requirements

Use of Grouper API Client assumes [PHP ](https://php.net/) and [Composer](https://getcomposer.org/) in the environment.
Grouper API Client has been tested successfully with PHP versions 5.5.9+ and 7.0+

## Installation

### Direct installation from Git
1. Clone the git repository
2. Install dependencies

```sh
$ git clone <git-repo-url> <output-dir>
$ cd <output-dir>
$ composer install
```

### Installation as an Composer Package Dependency

At minimum, the following should be in to your `composer.json` file:
```json
{
    "require": {
        "uagradcoll/grouper-api-client": "0.1.0"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:uazgraduatecollege/grouper-api-client.git"
        }
    ]
}
```

## Usage

A GrouperApiClient object can be instantiated as follows:

```php
// What you do here may depend on how you do autoloading.
// If installed w/Composer, this should be sufficient.
use GrouperApiClient;

$myGac = new GrouperApiClien([
  'baseUrl' => 'https://api.grouper.my.edu/grouper-ws/servicesRest/json/v2_1_005',
  'auth_user' => 'a-user-name',
  'auth_pw' => 'some_V3Ry-st0nG:ThiN6'
]);
```

Grouper API Client implements the following methods:

### GrouperApiClient::getMembers($groupId)
```php
$members = $myGac->getMembers('my.edu:aStem:aGroup');
print_r($members);

/* Output:
Array
(
    [people] => Array
        (
            [0] => 012345678909
            [1] => 123456789098
        )

    [groups] => Array
        (
          [0] => a0ccc9afa7584d369fb84172518fdba0
        )
)
*/
```

### GrouperApiClient::hasMember($groupId, $subjectId)
```php
$hasMember = $myGac->hasMember(
    'my.edu:aStem:aGroup',
    '012345678909'
);
print_r($hasMember);

/* Output:
1
*/
```

### GrouperApiClient::getMemberships($subjectId)
```php
$getMemberships = $myGac->getMemberships('012345678909');
print_r($getMemberships);

/* output
Array
(
    [subject] => 012345678909
    [groups] => Array
        (
            [0] => Array
                (
                    [name] => 'my.edu:aStem:aGroup'
                    [uuid] => a0ccc9afa7584d369fb84172518fdba0
                )
        )
)
*/
```

### GrouperApiClient::addToGroup($groupId, $subjectId)
```php
$addToGroup = $myGac->addToGroup(
    'my.edu:aStem:aGroup',
    '012345678909'
);
print_r($addToGroup);

/* Output
Array
(
    [success] => 1
    [message] => SUCCESS: Member added
    [subject] => 012345678909
    [group] => my.edu:aStem.aGroup
)
*/
```

### GrouperApiClient::removeFromGroup($groupId, $subjectId)
```php
$removeFromGroup = $myGac->removeFromGroup(
    'my.edu:aStem:aGroup',
    '012345678909'
);
print_r($removeFromGroup);

/* Output
Array
(
    [success] => 1
    [message] => SUCCESS: Member was removed or was not a member
    [subject] => 012345678909
    [group] => my.edu:aStem:aGroup
)
*/
```

## About

Grouper API Client for PHP is based on [GrouperClient JS](https://bitbucket.org/uazgraduatecollege/grouperclient-js).


## License

Copyright (c) 2018 Arizona Board of Regents on behalf of the University of Arizona, all rights reserved


## Status

A work in-progress, may be `unstable`. Contributors welcome if you bring us a shrubbery.
